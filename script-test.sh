#!/bin/bash

value=$(cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | head --bytes 1)
value2=$(cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | head --bytes 2)
text1=$(shuf text.txt | head -n 2)
text6=$(shuf text.txt | head -n 15)
name=$(shuf -n1  /usr/share/dict/words)

convert car2.png -density 300 -font Ubuntu -pointsize 8 -draw "text 110,120 '$value'" \
       -density 300 -font Ubuntu -pointsize 8 -draw "text 720,120 '$value2'" \
       -density 300 -font Ubuntu -pointsize 8 -draw "text 180,85 '$name'" \
       -density 300 -font Ubuntu -pointsize 6 -draw "text 80,175 '$text1'" \
       -density 300 -font Ubuntu -pointsize 6 -draw "text 80,57 '$text6'" \
       TextImage_${RANDOM}.png
    